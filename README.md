# Sonic v1.2.0 

Multipurpose bot for Discord.

# Bot Statistics
[![Discord Bots](https://discordbots.org/api/widget/463705240257167361.svg)](https://discordbots.org/bot/463705240257167361)

# Config Variables

Note: This variables can be only used in join & leave messages. (coming soon)

{user} - indicates the member in format Username#0000.

{member} - indicates the member and @mentions the member.

{members} - displays the member count for a server.

{server} - displays the server name.

# Getting Support

If you need any help for the bot, feel free to join [Sonic's Support Server!](https://discord.gg/uCsMFsd)

# Additional Info

1. Sonic is not open source, this is just a repository for documentation. We have a tag in [Sonic's Server](https://discord.gg/uCsMFsd) called opensource, so if you need the detailed reason, join the server and type `;tag opensource` in #commands channel.
2. If you want to support Sonic's Development, feel free to upvote on [Discord Bot List!](https://discordbots.org/bot/463705240257167361")
3. If you're interesed to donate, contact andrexyy#3986 via DM.